/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.ocjp;

/**
 *
 * @author molin
 */
public class Futbolista extends Persona{
    
        public static int TotalFutbolistas=0;//solo puede ser accedido por 
        protected int numero;
	protected Demarcacion demarcacion; //mesclamos los dos Enums 
	private Equipo equipo;

	public Futbolista() {
            TotalFutbolistas++;
	}

        
	public Futbolista(String nombre, int numero, int edad,Demarcacion demarcacion, Equipo equipo) {
		this.numero = numero;
		this.Nombre = nombre;
                this.edad = (short)edad;
		this.demarcacion = demarcacion;
		this.equipo = equipo;
	}
        //Hereda de clase abstracta debe implementarse
        
        @Override//sobreescritura
        public void entrenarse() { } // CLASE PADRE PROTECTED, CLASE HIJA PUBLIC
        //Sobrecargas
        public boolean entrenarse(float horas) {
         
            return ((int)horas > 10);
        }
        
	// Metodos getter y setter

        public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
        }

        
        
    //   
    @Override
    public void viaja() {
        
    }

    @Override
    public void alimenta() {
        
    }
        
	@Override
	public String toString() {
		return this.numero + " - " + this.Nombre + " - " + this.edad + " - "
                        + this.demarcacion.name() + " - " + this.equipo.getNombreCLUB();
	}

}