/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.ocjp;

/**
 *
 * @author molin
 */

//Uso de final 
public class Pronostico {
    
    public static final int maxima=80;
    public static final int media=50;
    public static final int minima=20;
    //Calcular probabilidad de victoria con proabailidades independientes
    public double probabilidadDeGanar(int posicion, int posicionRival)
    {
        int probabilidadEquipoA=getProbabilidad(posicion);
        int probabilidadEquipoB=getProbabilidad(posicionRival);
        System.out.println("Probabildiad Equipo1:"+probabilidadEquipoA+"Probabilidad Equipo2:"+probabilidadEquipoB);
        int probabilidadComplementaria=getProbabilidadComplementaria(probabilidadEquipoB);
        if(probabilidadComplementaria<0)
        {
           probabilidadComplementaria=probabilidadComplementaria*-1; 
        }
        System.out.println("Prob compl"+probabilidadComplementaria);
        double a,b, probabilidad;
        a = ((double)probabilidadEquipoA/100);
        System.out.println("A:"+a);
        b=((double)probabilidadComplementaria/100);
        System.out.println("B:"+b);
        probabilidad=a*b;
        return probabilidad;
        //return 0;
    }

    //Uso de bitwise,uso de casting
    public  int getProbabilidadComplementaria(int probabilidad)
    {
        byte complemento=(byte) probabilidad;
        complemento=(byte) (complemento-100);
        System.out.println("Probabilidad contraria: "+complemento);
        return complemento;
    }
    
    //
    public void publico(){
    
        String salida ="";
        int a = 60;	/* 60 = 0011 1100 */  
     int b = 13;	/* 13 = 0000 1101 */
     int c = 0,d,e,f;

     c = a & b;       /* 12 = 0000 1100 */

     d = a | b;       /* 61 = 0011 1101 */
     

     e = a ^ b;       /* 49 = 0011 0001 */

     f = ~a;          /*-61 = 1100 0011 */
     salida+=c+d*e+f;
        System.out.println("Total Asistentes: "+salida);
    }
    
    public int getProbabilidad(int posicion)
    {
        if(posicion>0 && posicion<5)
        {
            return maxima;
        }
        
        else if(posicion>4 && posicion <9)
        {
            return media;
        }
        else if(posicion>8 && posicion<13)
        {
            return minima;
        }
        else 
        {
            System.out.println("Posicion del equipo no valida");
            return 0;
        }
    }
}

