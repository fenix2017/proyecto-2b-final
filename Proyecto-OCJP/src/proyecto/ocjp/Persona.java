/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.ocjp;

/**
 *
 * @author Buhobit
 */
public abstract class Persona implements Individuo{
    //Atributos privados no van a heredar
    //Atributos protected son heredados y únicamente usados por clases hijas
    protected String Nombre;
    protected short edad;
    
    //Toda clase hija esta obligada a implementar el metodo entrenarse
    protected abstract void entrenarse();
    /*  P R O T E C T E D   */ 
    
    public Persona() {
    }

    public Persona(String Nombre, short edad) {
        this.Nombre = Nombre;
        this.edad = edad;
    }
    
    //sobrecarga constructores
    public Persona(int edad) {
    //Casting variables primitivas 
        this.edad = (short)edad;
    } 

    @Override
    public String toString() {
        String salida="\n"+this.Nombre+" - Edad -"+this.edad;
        System.out.println(""+salida);
        return salida;
    }
    
    
}
