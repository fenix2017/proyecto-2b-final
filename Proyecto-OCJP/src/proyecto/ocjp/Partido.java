/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.ocjp;

/**
 *
 * @author Buhobit
 */
public class Partido {

    ListaSeleccionados lista1 = new ListaSeleccionados();
    ListaSeleccionados lista2 = new ListaSeleccionados();
    
    public void equipoA (){
    
    Equipo a = Equipo.BARCELONA;
    a.toString();
    Entrenador dta = new Entrenador("Juega muy Ofensivamente!!");
    dta.Nombre="Guillermo Almada";
    dta.edad=(int)47.00;
    dta.alimenta();
    
    lista1.addEntrenador(dta);
    
    Futbolista f1 = new Futbolista();
    f1.setEquipo(a);
    f1.Nombre= "Maximo Banguera";
    f1.edad=25;
    f1.numero=0;
    f1.demarcacion=Demarcacion.PORTERO;
    lista1.addJugador(f1);
    
    Futbolista f2 = new Futbolista();
    f2.setEquipo(a);
    f2.Nombre= "Jhonatan Alvez";
    f2.edad=29;
    f2.numero=44;
    f2.demarcacion=Demarcacion.DELANTERO;
    lista1.addJugador(f2);
    
    lista1.toString();
    
    //C A S T I N G    D E   R E F E R E N C I A
    Persona per = (Persona)f2;
    System.out.println("Goleador del campeonato!!!: "+per.toString());
    
    
    System.out.println("Entreno?: "+f1.Nombre+" - "+ f1.entrenarse(12));
    System.out.println("Entreno?: "+f2.Nombre+" - "+ f1.entrenarse(12));
    System.out.println("Total jugadores: "+Futbolista.TotalFutbolistas+"\n===");
    }

    
    public void equipoB (){
    //limpiar conteo
    Futbolista.TotalFutbolistas=0;
    
    
    Equipo b = Equipo.LIGA_DE_QUITO;
    b.toString();
    Entrenador dta = new Entrenador("Juega al contragolpe!!!");
    dta.Nombre="Gustavo Mun'ua";
    dta.edad=(int)45.00;
    dta.alimenta();
    
    lista2.addEntrenador(dta);
    
    Futbolista f1 = new Futbolista();
    f1.setEquipo(b);
    f1.Nombre= "Sebas Vitery";
    f1.edad=27;
    f1.numero=1;
    f1.demarcacion=Demarcacion.PORTERO;
    
    
    lista2.addJugador(f1);
    
    Futbolista f2 = new Futbolista();
    f2.setEquipo(b);
    f2.Nombre= "Hernan Barcos";
    f2.edad=32;
    f2.numero=10;
    f2.demarcacion=Demarcacion.DELANTERO;
    lista2.addJugador(f2);
    lista2.toString();
    
    System.out.println("Entreno?: "+f1.Nombre+" - "+ f1.entrenarse(12));
    System.out.println("Entreno?: "+f2.Nombre+" - "+ f1.entrenarse(12));
    
    //C O N V E R S I O N    D E   R E F E R E N C I A
    Object per = f2;
        System.out.println("Capitan de liga: "+per.toString());
    
    System.out.println("Total jugadores: "+Futbolista.TotalFutbolistas);
    }
}