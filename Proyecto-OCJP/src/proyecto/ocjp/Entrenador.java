/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.ocjp;

/**
 *
 * @author Buhobit
 */
public class Entrenador extends Persona {

    public String descripcion;

    public Entrenador() {
    }

    public Entrenador(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    @Override
    public void entrenarse() {
        
    }
    @Override
    public void viaja() {
       
    }

    @Override
    public void alimenta() {
       
    }

    @Override
    public String toString() {
        String salida = "";
        
        salida+=this.Nombre+" - "+ this.edad +" - " +this.descripcion;
        return salida;
    }
    
    
    
}
